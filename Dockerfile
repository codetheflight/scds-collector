FROM alpine:3.10

RUN apk update && apk add wget openjdk11-jre zip tini

COPY target/scdsingest-jar-with-dependencies.jar /scds/ingester.jar
COPY run.sh /scds/run.sh
WORKDIR /scds

# And run it directly, we don't even need a shell wrapper at this point
ENTRYPOINT [ "/sbin/tini", "--" ]
CMD [ "/scds/run.sh" ]
