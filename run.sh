#!/bin/sh

if [ "x$SCDS_CONFIG_FILE" != "x" ]; then
    # Assume the config file exists, and use that instead of relying on environment vars
    # The config file can be volume-mounted in when running docker
    test -f ${SCDS_CONFIG_FILE}
    java -Dconfig.file=${SCDS_CONFIG_FILE} -jar /scds/ingester.jar
    exit 0
fi

# We're expecting the caller to set environment (e.g. in a GKE environment)
# Ensure that the parameters we need exist
test ${SCDS_PROVIDER_URL:?}
test ${SCDS_QUEUE:?}
test ${SCDS_CONNECTION_FACTORY:?}
test ${SCDS_USERNAME:?}
test ${SCDS_PASSWORD:?}
test ${SCDS_VPN:?}

if [ "x$SCDS_BUCKET_NAME" == "x" ]; then
    # Assume this is a local logging setup
    test ${SCDS_LOG_DIR:?}
    SCDS_OUTPUT_PARAM=logDir
    SCDS_OUTPUT_VALUE=$SCDS_LOG_DIR
    SCDS_OUTPUT_MODULE=local.notams.scdsingest.outputs.MessageFileOutput
else
    SCDS_OUTPUT_PARAM=gcsBucketName
    SCDS_OUTPUT_VALUE=$SCDS_BUCKET_NAME
    SCDS_OUTPUT_MODULE=local.notams.scdsingest.outputs.GcsFileOutput
fi

java \
    -DproviderUrl=${SCDS_PROVIDER_URL}               \
    -Dqueue=${SCDS_QUEUE}                            \
    -DconnectionFactory=${SCDS_CONNECTION_FACTORY}   \
    -Dusername=${SCDS_USERNAME}                      \
    -Dpassword=${SCDS_PASSWORD}                      \
    -Dvpn=${SCDS_VPN}                                \
    -Dmetrics=false                                  \
    -Doutput=${SCDS_OUTPUT_MODULE}                   \
    -D${SCDS_OUTPUT_PARAM}=${SCDS_OUTPUT_VALUE}      \
    -jar /scds/ingester.jar
