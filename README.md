# Using the consumer
This is an implementation around the Solace JMS Consumer to connect to the [SCDS](https://scds.swim.faa.gov) NOTAM data feed.

## Configuration Options
Set the following configuration options, either via an environment variable (if you plan to run Docker or the `run.sh` script),
via a config file (and use the `java -Dconfig.file=/path/to/settings.conf -jar ingester.jar` format), 
or as plain command line options (via `-Doption=value`) when running Java. 

Options to be set via environment variable are all `SCDS_*` (the ones in parentheses). 

### SCDS provided values (always required)

- `providerUrl` (`SCDS_PROVIDER_URL`): URL of the message broker including the port (e.g. tcps://hostname:55443)
- `queue` (`SCDS_QUEUE`): Name of the queue to connect to receive data
- `connectionFactory` (`SCDS_CONNECTION_FACTORY`): Connection Factory; typically this will be `username.CF`
- `username` (`SCDS_USERNAME`): Username for authentication
- `password` (`SCDS_PASSWORD`): Password for authentication
- `vpn` (`SCDS_VPN`): the message vpn to connect to on the broker
- `metrics` Set it to `false`, metrics aren't implemented (but you still need to set this, because the Solace consumer expects it). If you're using `run.sh`, it is already set to `false`

### Your choice of values

If you use the config file, or define the options manually, use the following:

- `output`: Pick between
  - `local.notams.scdsingester.outputs.MessageFileOutput` (the full classpath), and set the `logDir` option at the same time. This logs the messages, one per file, to the `logDir`.
  - `local.notams.scdsingester.outputs.GcsMessageOutput` (the full classpath), and set the `gcsBucketName` option to the appropriate bucket (e.g. `foo.appspot.com`). 

If you use `run.sh` or Docker:

- Set `SCDS_BUCKET_NAME`, this implies using GCS (this is equivalent to `output=[...].GcsMessageOutput gcsBucketName=$SCDS_BUCKET_NAME`). 
- Set `SCDS_LOG_DIR` (**and** the do not set `SCDS_BUCKET_NAME`), this implies local logging (and is equivalent to `output=[...].MessageFileOutput logDir=$SCDS_LOG_DIR`)

## Using run.sh

`run.sh` is a convenient way to run the ingestion locally. It will handle converting the aforementioned
environment variables into the appropriate command line options to the ingester. Setting the `SCDS_CONFIG_FILE`
environment variable to the appropriate config file lets you skip setting the other variables, as it is expected
that the configuration is read from the file. (This is equivalent to running `java -Dconfig.file=/x/y/z.conf -jar ingester.jar` directly)

## Running the docker image

The docker image ultimately runs `run.sh`, so the same rules apply. The main thing to watch for is to make sure you have
bind-mounted the config file and log dir into the image. (_If you have no clue what this means, you need to learn Docker some._)

## Running locally, writing to GCS

When developing or debugging, it's useful to be able to write directly to GCS. To do this, the `GOOGLE_APPLICATION_CREDENTIALS` 
environment needs to be set (set it in the Docker image via `-e G_A_C=/path/to/key`), which points to the JSON key for your 
Google Cloud service account. The service account used to write to GCS should have at least the following IAM roles 
associated with it:
    - `resourcemanager.projects.get`
    - `storage.buckets.get`
    - `storage.buckets.list`
    - `storage.objects.create`


## Sample config file

Config File Example (application.conf)
```
providerUrl: https://foo.faa:12345/
queue: bar
connectionFactory: yankee.zulu.CF
username: yankee.zulu
password: hunter2
vpn: victornovember
metrics:false
output:local.notams.scdsingester.outputs.MessageFileOutput
logDir:./log/
```
