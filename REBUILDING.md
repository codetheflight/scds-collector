# Rebuilding the container

## Pre-requisites

- Java 8 or higher (Java 12 - the latest as of this writing - works fine)
- Maven
- Docker (to rebuild the docker image)

## Rebuild the JAR

Rebuild the Java code, and package it up with something like `mvn package -f "/PATH/TO/scds-ingest/pom.xml`.

## Build the Docker image

Run `docker build .` (and pass in any other options that are necessary for your environment, a `-t [tag]` can be particularly useful)
