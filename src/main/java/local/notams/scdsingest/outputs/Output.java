package local.notams.scdsingest.outputs;

import com.typesafe.config.Config;

public abstract class Output {
    private final Config config;

    public Output(Config config) {
        this.config = config;
    }

    public abstract void output(String message);

    // The original SCDS jumpstart allows converting to JSON, but it's 
    // JSON with embedded XML tags, which is really really nasty.
    protected String convert(String message) {
            return message;
    }
}
