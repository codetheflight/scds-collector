package local.notams.scdsingest.outputs;

import com.typesafe.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.storage.*;

public class GcsFileOutput extends Output {
    private static final Logger logger = LoggerFactory.getLogger(MessageFileOutput.class);
    private final Storage storage;
    private final Bucket bucket;
    private final String bucketName;

    public GcsFileOutput(Config config) {
        super(config);
        // This relies on the GOOGLE_APPLICATION_CREDENTIALS environment variable
        // This will need to change in a prod environment (don't want my keys in code!)
        logger.info("Connecting to GCS");
        storage = StorageOptions.getDefaultInstance().getService();
        bucketName = config.getString("gcsBucketName");
        logger.info("Connected to GCS, looking for bucket {}", bucketName);

        bucket = storage.get(bucketName);
        if (bucket.exists()) {
            logger.info("Found GCS bucket {}", bucket.getName());
        } else {
            logger.error("Didn't find GCS bucket {}", bucketName);
        }
    }

    @Override
    public void output(String message) {
        String filename = new StringBuilder().append("scds-incoming/NOTAM-").append(System.currentTimeMillis()).append(".xml").toString();
        logger.info("Writing to file gcs://{}/{}", bucketName, filename);
        byte[] bytes = message.getBytes();
        logger.info("I have {} bytes to write from the message", bytes.length);
        if (bytes.length > 0) {
            Blob blob = bucket.create(filename, bytes);
            logger.info("Created blob {}", blob.getBlobId());
        } else {
            logger.info("Skipped empty message");
        }
    }
}
