package local.notams.scdsingest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import local.notams.scdsingest.outputs.Output;
import local.notams.scdsingest.solace.JMSConsumer;
import local.notams.scdsingest.solace.SolaceConsumer;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.codahale.metrics.MetricRegistry;


/**
 * Hello world!
 *
 */
public class Ingester 
{
    private static final Logger logger = LoggerFactory.getLogger(Ingester.class);
    private static final MetricRegistry metrics = new MetricRegistry();

    public static void main(String[] args) throws Exception {
        Output reporter = null;
        Config config = ConfigFactory.load();
        String reporterName = config.getString("output");
        try {
            Class<?> clazz = Class.forName(reporterName);
            Object obj = clazz.getConstructor(Config.class).newInstance(config);
            reporter = (Output) obj;
        } catch (Exception e) {
            logger.error("Invalid outputs class provided {} - {}", reporterName, e.getMessage());
            System.exit(-1);
        }

        SolaceConsumer consumer = new JMSConsumer(config, metrics, reporter);
        consumer.connect();

        while (true) {
            Thread.sleep(20000);
        }
    }

}
